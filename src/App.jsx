import React from "react"
import './styles/app.scss'

// export function App() {
//     return(
//         <div className="App">
//             <header className="App-header">
//                 <img src={logo} className="App-logo" alt="logo"/>
//                 <p>
//                     Edit <code>src/App.js</code> and save to reload.
//                 </p>
//                 <a className="App=link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
//                     Learn React
//                 </a>
//             </header>
//         </div>
//     );
// }
export function Header() {
    return(
        <header>
            <section className="product-view">
                <div className="row">
                    <div className="col-6 p-0" >
                        <ul className="product-view__list text-light">
                            <li className="m-1"><i className="fas fa-check pr-2"></i>Put on this page information about your product</li>
                            <li className="m-1"><i className="fas fa-check pr-2"></i>A detailed description of your product</li>
                            <li className="m-1"><i className="fas fa-check pr-2"></i>Tell us about the advantages and merits</li>
                            <li className="m-1"><i className="fas fa-check pr-2"></i>Associate the page with the payment system</li>
                        </ul>
                    </div>
                    <div className="col-6 p-0">
                        <div className="bg-white product-view__box" ></div>
                    </div>
                </div>
            </section>
        </header>
    );
}
export function About() {
    return(
        <section className="product-about">
            <div className="row">
                <div className="col-7 pr-5">
                    <h2>About your product</h2>
                    <br/>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. </p>

                    <p>Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.</p>
                </div>
                <div className="col-5 product-about__box">

                </div>
            </div>
        </section>
    );
}
export function Pluses() {
    return(
        <section className="section-pluses">
            <h2 className="text-center mb-4">Dignity and pluses product</h2>
            <div className="row pt-2">
                <div className="col d-flex">
                    <i className="fas fa-plus-square"></i>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                </div>
                <div className="col d-flex">
                    <i className="fas fa-plus-square"></i>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                </div>
            </div>
            <div className="row">
                <div className="col d-flex">
                    <i className="fas fa-plus-square"></i>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                </div>
                <div className="col d-flex">
                    <i className="fas fa-plus-square"></i>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                </div>
            </div>
            <div className="row">
                <div className="col d-flex">
                    <i className="fas fa-plus-square"></i>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                </div>
                <div className="col d-flex">
                    <i className="fas fa-plus-square"></i>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                </div>
            </div>
        </section>
    );
}
export function Screenshots() {
    return(
      <section className="section-screenshots">
          <h2 className="text-center mb-4">Screenshots</h2>
          <div className="row pt-2 pb-2">
              <div className="col row">
                  <div className="col-4 section-screenshots__box"></div>
                  <div className="col-8">
                      <h3>The description for the image</h3>
                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                  </div>
              </div>
              <div className="col row">
                  <div className="col-4 section-screenshots__box"></div>
                  <div className="col-8">
                      <h3>The description for the image</h3>
                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                  </div>
              </div>
          </div>
          <div className="row pt-3">
              <div className="col row">
                  <div className="col-4 section-screenshots__box"></div>
                  <div className="col-8">
                      <h3>The description for the image</h3>
                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                  </div>
              </div>
              <div className="col row">
                  <div className="col-4 section-screenshots__box"></div>
                  <div className="col-8">
                      <h3>The description for the image</h3>
                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                  </div>
              </div>
          </div>
      </section>
    );
}
export function Reviews() {
    return(
        <section className="section-reviews">
            <h2 className="text-center mb-4">Reviews</h2>
            <div className="row pt-2 pb-3">
                <div className="col row">
                    <div className="col-md-auto mr-4 ml-4 section-reviews__avatar"></div>
                    <div className="col section-reviews__message">
                        <p className="font-italic mb-2">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                        <p className="text-muted mb-2">Lourens S.</p>
                    </div>
                </div>
                <div className="col row">
                    <div className="col-md-auto mr-4 ml-4 section-reviews__avatar"></div>
                    <div className="col section-reviews__message">
                        <p className="font-italic mb-2">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                        <p className="text-muted mb-2">Lourens S.</p>
                    </div>
                </div>
            </div>
            <div className="row pt-2 pb-3">
                <div className="col row">
                    <div className="col-md-auto mr-4 ml-4 section-reviews__avatar"></div>
                    <div className="col section-reviews__message">
                        <p className="font-italic mb-2">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                        <p className="text-muted mb-2">Lourens S.</p>
                    </div>
                </div>
                <div className="col row">
                    <div className="col-md-auto mr-4 ml-4 section-reviews__avatar"></div>
                    <div className="col section-reviews__message">
                        <p className="font-italic mb-2">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                        <p className="text-muted mb-2">Lourens S.</p>
                    </div>
                </div>
            </div>
        </section>
    )
}
export function Sell() {
    return(
        <section className="section-sell">
            <h2 className="text-center mb-4">Buy it now</h2>
            <div className="row">
                <div className="position-relative col text-light p-0 section-sell__product mr-3 ml-3">
                    <h3 className="text-center m-3">Standart</h3>
                    <div className="bg-white text-center">
                        <p className="section-sell__product_price">
                            $100
                        </p>
                    </div>
                    <ol className="text-light">
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Consectetuer adipiscing elit</li>
                        <li className="m-1">Aenean commodo ligula</li>
                        <li className="m-1">Eget dolor aenean massa</li>
                    </ol>
                    <input type="button" className="text-center bg-white section-sell__product_btn" value="BUY"/>
                </div>
                <div className="position-relative col text-light p-0 section-sell__product mr-3 ml-3">
                    <h3 className="text-center m-3">Premium</h3>
                    <div className="bg-white text-center">
                        <p className="section-sell__product_price">
                            $150
                        </p>
                    </div>
                    <ol className="text-light">
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Consectetuer adipiscing elit</li>
                        <li className="m-1">Aenean commodo ligula</li>
                        <li className="m-1">Eget dolor aenean massa</li>
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Consectetuer adipiscing elit</li>
                        <li className="m-1">Aenean commodo ligula</li>
                        <li className="m-1">Eget dolor aenean massa</li>
                    </ol>
                    <input type="button" className="text-center bg-white section-sell__product_btn" value="BUY"/>
                </div>
                <div className="position-relative col text-light p-0 section-sell__product mr-3 ml-3">
                    <h3 className="text-center m-3">Lux</h3>
                    <div className="bg-white text-center">
                        <p className="section-sell__product_price">
                            $200
                        </p>
                    </div>
                    <ol className="text-light">
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Consectetuer adipiscing elit</li>
                        <li className="m-1">Aenean commodo ligula</li>
                        <li className="m-1">Eget dolor aenean massa</li>
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Consectetuer adipiscing elit</li>
                        <li className="m-1">Aenean commodo ligula</li>
                        <li className="m-1">Eget dolor aenean massa</li>
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Consectetuer adipiscing elit</li>
                        <li className="m-1">Aenean commodo ligula</li>
                        <li className="m-1">Eget dolor aenean massa</li>
                    </ol>
                    <input type="button" className="text-center bg-white section-sell__product_btn" value="BUY"/>
                </div>
            </div>

        </section>
    )
}
export function Footer() {
    return(
        <footer>
            <section className="section-contacts text-light">
                <h2 className="text-center mb-4">Contacts</h2>
                <div className="row">
                    <div className="col-8 ">
                        <input type="text" className="section-contacts__input mb-4" placeholder="Your name:"/>
                        <input type="text" className="section-contacts__input mb-4" placeholder="Your email:"/>
                        <textarea type="text" className="section-contacts__textarea mb-3" placeholder="Your message:"></textarea>
                        <div className="d-flex justify-content-center">
                            <input type="button" className="text-center bg-white section-contacts__btn" value="SEND"/>
                        </div>
                    </div>
                    <div className="col-4 section-contacts__info">
                        <p className="mb-3"><i className="fab fa-skype"></i> here_your_login_skype</p>
                        <p className="mb-3"><i className="fas fa-flower"></i> 24758452659</p>
                        <p className="mb-3"><i className="fas fa-at"></i> asdfghjk@mail.ru</p>
                        <p className="mb-4"><i className="fas fa-phone"></i> 80 00 4568 55 55</p>
                        <span className="section-contacts__icons">
                            <i className="mr-2 fab fa-twitter"></i>
                            <i className="mr-2 fab fa-facebook-f"></i>
                            <i className="mr-2 fab fa-invision"></i>
                            <i className="mr-2 fab fa-glide-g"></i>
                            <i className="fab fa-youtube"></i>
                        </span>
                    </div>
                </div>
            </section>
        </footer>
    )
}