import React from 'react'
import ReactDOM from 'react-dom'

import {Header} from './App';
import {About} from './App';
import {Pluses} from './App';
import {Screenshots} from "./App";
import {Reviews} from "./App";
import {Sell} from "./App";
import {Footer} from "./App";
import './styles/main.scss';

ReactDOM.render(
<React.StrictMode>
    <Header />
    <main>
        <About/>
        <Pluses/>
        <Screenshots/>
        <Reviews/>
        <Sell/>
    </main>
    <Footer/>
</React.StrictMode>,
document.getElementById('root')
)
