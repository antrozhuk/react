const fs = require('fs');
const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const {BundleAnalyzerPlugin} = require('webpack-bundle-analyzer');

const ROOT_DIR = fs.realpathSync(process.cwd());
const isDev = process.env.NODE_ENV !== 'production';
const analyse = process.argv.includes('--analyse')

function fileName(file){
    return `[name].[${isDev ? 'hash:8' : 'contenthash'}].bundle.${file}`;
}

console.log("BUILD_MODE: ",process.env.NODE_ENV ? process.env.NODE_ENV : "development");

module.exports={
    mode: isDev ? "development" : "production",
    entry: {
        main: './src/index.jsx',
    },
    output: {
        filename: fileName('js'),
        path: path.resolve(ROOT_DIR, 'dist')
    },
    resolve: {
        extensions: ['.js','.json','.jsx']
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: [/node_modules/],
                use: ['babel-loader'],
            },
            {
                test: /\.(scss)$/,
                use: [MiniCssExtractPlugin.loader,'css-loader','sass-loader']
            },
            {
                test: /\.(png|jpe?g|gif|svg)$/i,
                use: [
                    {
                        loader: 'file-loader',
                    },
                ]
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: path.resolve(ROOT_DIR,'public/index.html'),
            chunks: ['main']
        }),
        new MiniCssExtractPlugin({
            filename:'css/[name].css'
        }),
        analyse && new BundleAnalyzerPlugin()
    ].filter(Boolean),
    devServer: {
        port:"3000",
        open:true
    }
}